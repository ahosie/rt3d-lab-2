#include <GL/glew.h>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <string>

#define RT3D_VERTEX 0
#define RT3D_COLOUR 1
#define RT3D_NORMAL 2
#define RT3D_TEXCOORD   3

namespace rt3d {
  void exitFatalError(char *message);
  char* loadFile(char *fname, GLint &fSize);
  void printShaderError(GLint shader);
  GLuint initShaders(char *vertFile, char *fragFile);
  // Some methods for creating meshes
  // ... will deal with indexed meshes later
  GLuint createMesh(GLuint numVerts, GLfloat* vertices, GLfloat* colours, GLfloat* normals, GLfloat* texcoords);
  GLuint createMesh(GLuint numVerts, GLfloat* vertices);
  GLuint createColourMesh(GLuint numVerts, GLfloat* vertices, GLfloat* colours);
  void setUniform4fv(const GLuint program, const char* uniformName, const GLfloat *data);
  void drawMesh(GLuint mesh, GLuint numVerts, GLuint primitive); 
}